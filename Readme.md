
Welcome to Ewloe!
=================

Description
-----------

Ewloe is a Javascript mocking framework in the style of Rhino Mocks. It is capable of creating mocks for Javascript classes,
as well as instances of Javascript objects. To see how to use it check out the examples below. (All functions except for
tells work with both strict and dynamic mocks. Check out the tests for more examples).

Examples
--------

The examples below all work on the following object :

    function Person() {
        var name = "Bob Doe";
        var age = 21;

        this.getName = function() {
            return name;
        }

        this.setName = function(value) {
            name = value;
        }

        this.getAge = function() {
            return age;
        }

        this.setAge = function(value) {
            age = value;
        }
    }

Examples :

##1. Strict Mocks

Strict mocks work the same as they do in most other languages, in that they will fail if any call occurs on the mock
for which an explicit expectation does not exist. They can be useful to ensure that expensive calls only occur once.

### Creating a strict mock for a class.

    var mockControl = new MockControl();
    var personMock = mockControl.createStrictMock(Person);

### Creating a strict mock for an instance.

    var person = new Person();

    var mockControl = new MockControl();
    var personMock = mockControl.createStrictMock(person);

##2. Dynamic Mocks

Dynamic mocks differ from strict mocks in that they will not cause the test to fail if an unexpected call occurs. Using
dynamic mocks helps to make your tests less brittle.

### Creating dynamic mocks for a class

    var mockControl = new MockControl();
    var personMock = mockControl.createDynamicMock(Person);

### Creating dynamic mocks for an instance.

    var person = new Person();

    var mockControl = new MockControl();
    var personMock = mockControl.createDynamicMock(person);

##3. Setting expectations

    var mockControl = new MockControl();
    var personMock = mockControl.createStrictMock(Person);

    personMock.expects().getName();
    personMock.getName();

    mockControl.verify();

##4. Stubbing methods (Only available on dynamic mocks)

    var mockControl = new MockControl();
    var personMock = mockControl.createDynamicMock(Person);

    personMock.tells().getName().toReturn("Bob");
    personMock.getName(); //Returns Bob

    mockControl.verify();

##5. Returning values when a mocked function is called.

    var mockControl = new MockControl();
    var personMock = mockControl.createDynamicMock(Person);

    personMock.tells().getName().toReturn("Bob");
    personMock.getName(); //Returns Bob

    mockControl.verify();

##6. Returning a different value each time a mocked function is called (or until the array runs out of values).

    var mockControl = new MockControl();
    var personMock = mockControl.createDynamicMock(Person);

    personMock.tells().getName().toReturnNext(["Bob", "Jane"]);
    personMock.getName(); //Returns Bob
    personMock.getName(); //Returns Jane

    mockControl.verify();

##6. Throwing an exception when the method is called.

    var mockControl = new MockControl();
    var personMock = mockControl.createStrictMock(Person);

    personMock.expects().getName().toThrow(new Error("An error occurred"));
    personMock.getName(); //Throws exception

    mockControl.verify();

##7. Executing a function when the method is called

    var mockControl = new MockControl();
    var personMock = mockControl.createStrictMock(Person);

    personMock.expects().getAge().toExecute(function() { return 42; });
    personMock.getAge(); //Returns 42

    mockControl.verify();

##8. Specifying the number of times the method should be called.

    var mockControl = new MockControl();
    var personMock = mockControl.createStrictMock(Person);

    personMock.expects().setName("Bob").twice();

    mockControl.verify();

##9. Specifying an expectation where you are only worried about the type of argument

    var mockControl = new MockControl();
    var personMock = mockControl.createStrictMock(Person);

    personMock.expects().setName(Arg.isA(String));
    personMock.setName("Jane");

    mockControl.verify();

##9. Specifying an expectation where you want to execute a function to check the arguments match

    var mockControl = new MockControl();
    var personMock = mockControl.createStrictMock(Person);

    personMock.expects().setName(Arg.satisfies(function(actual) { return actual === "Bob"}));
    personMock.setName("Bob");

    mockControl.verify();

##9. Specifying an expectation where you want to check the argument matches a given object

    var mockControl = new MockControl();
    var personMock = mockControl.createStrictMock(Person);

    personMock.expects().setName(Arg.sameAs("Bob"));
    personMock.setName("Bob");

    mockControl.verify();


## License 

(The MIT License)

Copyright (c) 2009 Your Name &lt;gurrie@gmail.com&gt;

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.