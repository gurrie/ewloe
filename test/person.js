function Person() {
    this.getName = function() {
        return "Persons Name";
    };

    this.getAge = function() {
        return 12;
    };

    this.addChangeListener = function(callback){
        callback();        
    }
}