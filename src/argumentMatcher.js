function Arg(){}

function TypeArg(type) {
    this.expectedType = type;

    this.toString = function() {
        return type.name;
    };
}

function FunctionArg(predicate) {
    this.predicate = predicate;
}

function PropertiesMatch(expected) {
    this.expectedValues = expected;
}

Arg.isA = function(type) {
    return new TypeArg(type);
};

/**
 * Invokes the passed passed predicate with the arguments passed to the mocked function when it is invoked and returns
 * the predicates value, which should be true or false.
 *
 * @param predicate The predicate to invoke when the mocked function is invoked
 */
Arg.satisfies = function(predicate) {
    return new FunctionArg(predicate);
};

/**
 * Returns true if the passed object contains exactly the same number of properties, of the same name, with the
 * same values. It can be used to check that two objects are equal
 *
 * @param expected The object we expect to receive.
 */
Arg.sameAs = function(expected) {
    return new PropertiesMatch(expected);    
};

function ArgumentMatcher() {
    var typeMatchers = {};

    initMatchers();

    this.areEqual = function(expected, actual) {
        if (expected.length != actual.length) {
            return false;
            
        }
        return checkArguments(expected, actual);
    };

    function initMatchers() {
        typeMatchers[Array] = matchArrays;
        typeMatchers[TypeArg] = matchType;
        typeMatchers[FunctionArg] = matchPredicate;
        typeMatchers[PropertiesMatch] = matchProperties;
    }

    function checkArguments(expected, actual) {
        if (expected == null) {
            return actual == null;
        }

        var typeMatcher = typeMatchers[expected.constructor] || matchObjects;

        return typeMatcher(expected, actual);
    }

    function matchArrays(expected, actual) {
        if ((expected && !actual) || (!expected && actual)) {
            return false;
        }

        if (expected.length != actual.length) {
            return false;
        }

        for (var i = 0; i < expected.length; i++) {
            if (!checkArguments(expected[i], actual[i])) {
                return false;
            }
        }

        return true;
    }

    function matchObjects(expected, actual) {
        return expected == actual;
    }

    function matchType(expected, actual) {
        return expected.expectedType === actual.constructor;
    }

    function matchPredicate(expected, actual) {
        return expected.predicate(actual);
    }

    function matchProperties(expected, actual) {
        var expectedPropertyCount = MockHelper.numberOfProperties(expected.expectedValues);
        var actualPropertyCount = MockHelper.numberOfProperties(actual);

        if (expectedPropertyCount !== actualPropertyCount) {
            return false;
        }

        for (var propertyName in expected.expectedValues) {
            if (expected.expectedValues[propertyName] != actual[propertyName]) {
                return false;
            }
        }

        return true;
    }
}