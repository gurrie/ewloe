#require "tools/jsmin"

task :default => [:minify]

task :clean do
  puts "Cleaning"

  if (File.exist?("ewloe.js"))
    File.delete("ewloe.js")
  end
end

task :run_lint do
  puts "Running Javascript Lint"

  puts %x{./jsl -conf jsl.conf}

  raise "JavascriptLint check failed" unless $?.success?  
end

task :run_tests do
  puts "Running tests"

  puts %x{jspec run --rhino}

  raise "Tests failed" unless $?.success?
end

task :combine_files => [:clean, :run_lint, :run_tests] do
  puts "Combining files"
  
  File.open("ewloe.js", "w") do |output_file|
    Dir["src/*.js"].each do |src_file|
      f = File.open(src_file, 'r+');

      f.each_line do |line|
        output_file.write(line)
      end

      output_file.write("\n")
    end
  end
end

task :minify => [:combine_files] do
  puts "Minifying"

  puts %x{ruby tools/jsmin.rb < ewloe.js > ewloe-min.js}

  raise "Minification failed" unless $?.success?
end